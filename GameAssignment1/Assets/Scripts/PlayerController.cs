using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    public int points = 0;
    public int scoreToGoNextLevel = 0;
    public TMP_Text scoreText;
    public TMP_Text timerText;
    public TMP_Text coinCountText;
    public TMP_Text blueCoinCountText;
    public TMP_Text greenCoinCountText;
    public int coinCount;
    public int blueCoinCount;
    public int greenCoinCount;
    private double timer;
    public String nextLevel;
    public GameObject Capsule;
    GameObject[] coins;
    GameObject[] Bluecoins;
    GameObject[] Greencoins;
    public GameObject panel;
    public int health;

    // Start is called before the first frame update
    void Start()
    {
        coins = GameObject.FindGameObjectsWithTag("Coin");
        Bluecoins = GameObject.FindGameObjectsWithTag("BlueCoin");
        Greencoins = GameObject.FindGameObjectsWithTag("GreenCoin");
        Debug.Log(coins.Length);
        Debug.Log(Bluecoins.Length);
        Debug.Log(Greencoins.Length);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        timerText.text = Math.Round(timer, 2).ToString();

        if (Input.GetButtonDown("1Key"))
        {
            this.GetComponent<Animator>().Play("Mma Kick");
        }
        if (Input.GetButtonDown("2Key"))
        {
            this.GetComponent<Animator>().Play("Twist Dance");
        }
        if (Input.GetButtonDown("3Key"))
        {
            this.GetComponent<Animator>().Play("Taunt");
        }
        if (Input.GetButtonDown("4Key"))
        {
            this.GetComponent<Animator>().Play("Hip Hop Dancing");
        }
    }

    public void addPoints(int addPoints)
    {
        points += addPoints;
        Debug.Log(points.ToString());
        scoreText.SetText("Score: " + points.ToString());
        coins = GameObject.FindGameObjectsWithTag("Coin");
        Bluecoins = GameObject.FindGameObjectsWithTag("BlueCoin");
        Greencoins = GameObject.FindGameObjectsWithTag("GreenCoin");
        coinCountText.SetText(coinCount.ToString());
        blueCoinCountText.SetText(blueCoinCount.ToString());
        greenCoinCountText.SetText(greenCoinCount.ToString());
        if (coins.Length + Bluecoins.Length + Greencoins.Length == 1)
        {
            Vector3 capPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 10);
            Instantiate(Capsule, capPosition, Quaternion.identity);
        }
    }

    public void goToNextLevel()
    {
        if (points >= scoreToGoNextLevel)
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene(nextLevel);
        }
    }

    public void addCoin()
    {
        coinCount += 1;
    }

    public void addBlueCoin()
    {
        blueCoinCount += 1;
    }

    public void addGreenCoin()
    {
        greenCoinCount += 1;
    }

    public void OpenPanel()
    {
        if (panel != null)
        {
            panel.SetActive(true);
            FindObjectOfType<ScoreBoard>().displayCoins.SetText(coinCountText.text);
            FindObjectOfType<ScoreBoard>().displayBlueCoins.SetText(blueCoinCountText.text);
            FindObjectOfType<ScoreBoard>().displayGreenCoins.SetText(greenCoinCountText.text);
            FindObjectOfType<ScoreBoard>().finalScore.SetText("Final Score: " + scoreText.text);
            FindObjectOfType<ScoreBoard>().timer.SetText("Final Time: " + timerText.text);
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class PlayerHealth : MonoBehaviour
{
    public int maxHealth;
    private int currentHealth;
    public string scene;
    public TMP_Text healthText;

    // Start is called before the first frame update
    void Start()
    {
        resetHealth();
        healthText.SetText("Health: " + currentHealth.ToString() + "/" + maxHealth.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetHealth()
    {
        currentHealth = maxHealth;
    }

    public void takeDamage()
    {
        currentHealth--;
        healthText.SetText("Health: " + currentHealth.ToString() + "/" + maxHealth.ToString());
        if (currentHealth == 0)
        {
            SceneManager.LoadScene(scene);
        }
    }
}

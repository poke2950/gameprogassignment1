using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public int coinValue;
    public GameObject particles;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 10, 0, Space.World);
    }

    public void OnTriggerEnter(Collider other)
    {
        string coinType = this.gameObject.tag;
        if (other.tag == "Player")
        {
            switch (coinType)
            {
                case "Coin":
                    FindObjectOfType<PlayerController>().addCoin();
                    break;
                case "BlueCoin":
                    FindObjectOfType<PlayerController>().addBlueCoin();
                    break;
                case "GreenCoin":
                    FindObjectOfType<PlayerController>().addGreenCoin();
                    break;
            }

            FindObjectOfType<PlayerController>().addPoints(coinValue);
            Instantiate(particles, this.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}

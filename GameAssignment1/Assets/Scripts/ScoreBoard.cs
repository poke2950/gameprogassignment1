using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScoreBoard : MonoBehaviour
{
    public TMP_Text displayCoins;
    public TMP_Text displayBlueCoins;
    public TMP_Text displayGreenCoins;
    public TMP_Text timer;
    public TMP_Text finalScore;

    // Start is called before the first frame update
    void Start()
    {
        if (this != null)
        {
            this.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void nextLevel()
    {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        FindObjectOfType<PlayerController>().goToNextLevel();
        FindObjectOfType<PlayerFootSteps>().audioSource.Play();
    }
}

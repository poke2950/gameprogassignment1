using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFootSteps : MonoBehaviour
{
    StarterAssets.ThirdPersonController thirdPersonController;
    StarterAssets.StarterAssetsInputs StarterAssetsInputs;
    public AudioClip[] clips;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StarterAssetsInputs = GetComponent<StarterAssets.StarterAssetsInputs>();
        thirdPersonController = GetComponent<StarterAssets.ThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (thirdPersonController.Grounded == true && StarterAssetsInputs.move[0] + StarterAssetsInputs.move[1] != 0)
        {
            if(StarterAssetsInputs.sprint == true)
            {
                audioSource.Pause();
                audioSource.clip = clips[2];
                audioSource.Play();
            } else
            {
                audioSource.Pause();
                audioSource.clip = clips[0];
                audioSource.Play();
            }
        }
        else if (thirdPersonController.Grounded == false)
        {
            audioSource.Pause();
            audioSource.clip = clips[1];
            audioSource.Play();
        }
        else
        {
            audioSource.Pause();
        }
    }
}
